#include <sourcemod>
#include <tf2>
#include <tf2_stocks>

// Weapon Index IDs.
#define SLEEPER     230
#define FLARE        39
#define F_FLARE    1081
#define DETONATOR   351
#define SCORCH      740
#define MANMELTER   595
#define WRANGLER    140
#define F_WRANGLER 1086


public Plugin:myinfo = {
	name = "HvH weapon blocker",
	author = "J_Tanzanite",
	description = "Removes weapons that are bad for HvH.",
	version = "1.2.0",
	url = ""
};


public OnPluginStart()
{
	CreateTimer(0.5, Timer_WeaponCheck, _, TIMER_REPEAT);
}

public Action Timer_WeaponCheck(Handle timer)
{
	for (int i = 1; i <= MaxClients; i++)
	{
		if (IsPlayerValid(i))
		{
			if (IsPlayerAlive(i))
			{
				if (GetEntPropEnt(i, Prop_Send, "m_hActiveWeapon") != -1)
				{
					if (IsWeaponBanned(GetEntPropEnt(i, Prop_Send, "m_hActiveWeapon")))
					{
						TF2_RemoveWeaponSlot(i, IsWeaponBanned(GetEntPropEnt(i, Prop_Send, "m_hActiveWeapon")) - 1); // Remove the slot :)
						PrintCenterText(i, "This weapon is not allowed on this server, change your loadout to proceed HvHing.");
					}
				}
			}
		}
	}
}

// If the timer fucks up and doesn't remove the weapon - block the attack.
public Action OnPlayerRunCmd(int client, int& buttons, int& impulse, float vel[3], float angles[3], int& weapon, int& subtype, int& cmdnum, int& tickcount, int& seed, int mouse[2])
{
	if (!IsPlayerValid(client))
		return Plugin_Continue;

	if (IsPlayerAlive(client))
	{
		if (GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon") != -1)
			if (IsWeaponBanned(GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon")) && (buttons & IN_ATTACK))
				buttons &= ~IN_ATTACK;
	}

	return Plugin_Continue;
}

int IsWeaponBanned(int weapon)
{
	switch (GetEntProp(weapon, Prop_Send, "m_iItemDefinitionIndex"))
	{
		case SLEEPER: { return 1; }
		case FLARE: { return 2; }
		case F_FLARE: { return 2; }
		case DETONATOR: { return 2; }
		case SCORCH: { return 2; }
		case MANMELTER: { return 2; }
		case WRANGLER: { return 2; }
		case F_WRANGLER: { return 2; }
	}

	return 0;
}

bool IsPlayerValid(int client)
{
	return (client > 0 && client <= MaxClients && IsClientConnected(client) && IsClientInGame(client)) ? true : false;
}